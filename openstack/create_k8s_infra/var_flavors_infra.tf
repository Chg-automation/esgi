#######GLOBALE VARIABLE###################
variable "public_true" {
  default = "true"
}
variable "public_false" {
  default = "false"
}

#######FLAVOR KUBERNETES WORKERS##########

variable "cpus_k8sw" {
  default = "4"
}
variable "flavor_k8sw" {
  default = "flavor_k8sw"
}
variable "ram_k8sw" {
  default = "4096"
}
variable "disk_k8sw" {
  default = "20"
}

#######FLAVOR KUBERNETES MASTERS##########

variable "cpus_k8sm" {
  default = "4"
}
variable "flavor_k8sm" {
  default = "flavor_k8sm"
}
variable "ram_k8sm" {
  default = "4096"
}
variable "disk_k8sm" {
  default = "20"
}


########FLAVOR KUBERNETES ADMINISTRATOR######

variable "cpus_adm" {
  default = "4"
}
variable "flavor_adm" {
  default = "flavor_adm"
}
variable "ram_adm" {
  default = "4096"
}
variable "disk_adm" {
  default = "20"
}
