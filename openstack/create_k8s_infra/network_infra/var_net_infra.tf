variable "router_k8s" {
  default = "router_k8s"
}
variable "network_k8s" {
  default = "network_k8s"
}
variable "subnet_k8s" {
  default = "subnet_k8s"
}
variable "cidr" {
  default = "172.16.10.0/16"
}
variable "gatewayip" {
  default = "172.16.10.1"
}
variable "sg_k8s" {
  default = "sg_k8s"
}





