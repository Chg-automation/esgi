resource "openstack_networking_router_v2" "router_k8s" {
  name           = var.router_k8s
  admin_state_up = "true"
  external_network_id ="a6eb52df-7955-4a72-8330-712c6837b924"
}
resource "openstack_networking_network_v2" "network_k8s" {
  name           = var.network_k8s
  admin_state_up = "true"
}
resource "openstack_networking_subnet_v2" "subnet_k8s" {
  name       = var.subnet_k8s
  network_id = "${openstack_networking_network_v2.network_k8s.id}"
  cidr       = var.cidr
  ip_version = 4
  gateway_ip = var.gatewayip
}
resource "openstack_networking_router_interface_v2" "interface_k8s" {
  router_id = "${openstack_networking_router_v2.router_k8s.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_k8s.id}"
}
resource "openstack_networking_secgroup_v2" "sg_k8s" {
  name        = var.sg_k8s
  description = "My neutron security group"
}


##################SECURITY GROUPS#########################


resource "openstack_networking_secgroup_rule_v2" "sgrtcp_k8s_in" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "172.16.10.1/16"
  security_group_id = "${openstack_networking_secgroup_v2.sg_k8s.id}"
}
resource "openstack_networking_secgroup_rule_v2" "sgrtcp_k8s_out" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "172.16.10.1/16"
  security_group_id = "${openstack_networking_secgroup_v2.sg_k8s.id}"
}
resource "openstack_networking_secgroup_rule_v2" "sgrudp_k8s_in" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "172.16.10.1/16"
  security_group_id = "${openstack_networking_secgroup_v2.sg_k8s.id}"
}
resource "openstack_networking_secgroup_rule_v2" "sgrudp_k8s_out" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "172.16.10.1/16"
  security_group_id = "${openstack_networking_secgroup_v2.sg_k8s.id}"
}
