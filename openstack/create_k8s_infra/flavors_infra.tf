resource "openstack_compute_flavor_v2" "flavor_k8sw" {
  name  = var.flavor_k8sw
  ram   = var.ram_k8sw
  vcpus = var.cpus_k8sw
  disk  = var.disk_k8sw
  is_public = var.public_true

}

resource "openstack_compute_flavor_v2" "flavor_k8sm" {
  name  = var.flavor_k8sm
  ram   = var.ram_k8sm
  vcpus = var.cpus_k8sm
  disk  = var.disk_k8sm
  is_public = var.public_true

}
resource "openstack_compute_flavor_v2" "flavor_adm" {
  name  = var.flavor_adm
  ram   = var.ram_adm
  vcpus = var.cpus_adm
  disk  = var.disk_adm
  is_public = var.public_true
}



