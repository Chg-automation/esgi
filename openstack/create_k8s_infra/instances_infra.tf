resource "openstack_compute_instance_v2" "instances_k8sm" {
  for_each = var.nodes-k8sm
  name            = each.key
  image_name      = "${data.openstack_images_image_v2.focal.name}" 
  flavor_id       = "${openstack_compute_flavor_v2.flavor_k8sm.id}"
  key_pair        = "macbook"
  security_groups = ["default","${data.openstack_networking_secgroup_v2.sg_k8s.name}"]

  network {
    name = "${data.openstack_networking_network_v2.network_k8s.name}"
    fixed_ip_v4 = var.nodes-k8sm[each.key]
  }
}

resource "openstack_compute_instance_v2" "instances_k8sw" {
  for_each = var.nodes-k8sw
  name            = each.key
  image_name      = "${data.openstack_images_image_v2.focal.name}"
  flavor_id       = "${openstack_compute_flavor_v2.flavor_k8sw.id}"
  key_pair        = "macbook"
  security_groups = ["default","${data.openstack_networking_secgroup_v2.sg_k8s.name}"]

  network {
    name = "${data.openstack_networking_network_v2.network_k8s.name}"
    fixed_ip_v4 = var.nodes-k8sw[each.key]
  }
}
resource "openstack_networking_floatingip_v2" "floatingip_k8sm" {
  pool = "Pub_Net"
  address = "10.50.1.151"
  
}
