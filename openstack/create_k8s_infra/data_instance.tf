data "openstack_networking_network_v2" "network_k8s" {
  name = "network_k8s"
}

data "openstack_networking_secgroup_v2" "sg_k8s" {
  name = "sg_k8s"
}
