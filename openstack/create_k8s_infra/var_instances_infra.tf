variable "nodes-k8sm" {
    type = map
    default = {
        "k8sm1" = "172.16.1.10"
        "k8sm2" = "172.16.1.20"
        "k8sm3" = "172.16.1.30"
    }
}

variable "nodes-k8sw" {
    type = map
    default = {
        "k8sw1" = "172.16.1.11"
        "k8sw2" = "172.16.1.22"
        "k8sw3" = "172.16.1.33"
    }
}

variable "nodes-lb" {
    type = map
    default = {
        "lb1" = "172.16.1.10"
        "lb2" = "172.16.1.20"
    }
}
